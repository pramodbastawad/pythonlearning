class A:
    def fun(self):
        print("I am Class A method")

class B(A):
    def fun(self):
        print("I am Class B method")
        super().fun()
    

class C(A):
    def fun(self):
        print("I am Class C method")
        super().fun()

class D(B,C):
    def fun(self):
        print("I am Class D method")
        super().fun()
        # A.fun(self)