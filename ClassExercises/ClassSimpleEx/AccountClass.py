# Python class and object exercises
# The Account Class

# Class definition for Account
class Account:
    def __init__(self):
        self.aid = ''
        self.name = ''
        self.balance = 0.0

    def createAccount(self, nid, name, bal):
        self.aid = nid
        self.name = name
        self.balance = bal

    def getAccount(self):
        print("The Account details")
        print("Id: ", self.aid)
        print("Name:", self.name)
        print("Balance:", self.balance)

    def deposit(self, amount):
        self.balance += amount
        return self.balance

    def withdraw(self, amount):
        if amount >= self.balance:
            print("Insufficient balance. Withdrawal is not allowed")
            return None
        else:
            self.balance -= amount
        return self.balance

    def transfer(self, amount):
        if amount >= self.balance:
            print("Insufficient balance. Withdrawal is not allowed")
        else:
            self.balance -= amount
        return self.balance

if __name__ == '__main__':
    obj = Account()
    obj.createAccount("12345", "Pramod", 5000.00)
    obj.getAccount()

    amt = float(input("Enter the amount to withraw:"))
    amt = obj.withdraw(amt)
    if amt is not None:
        print("Amount Withdrwan. The balance is:", amt)
    obj.getAccount()

    amt = float(input("Enter the amount deposit:"))
    amt = obj.deposit(amt)
    print("Amount deposited. The balance is:", amt)
    obj.getAccount()

    amt = float(input("Enter the amount to transfer:"))
    amt = obj.transfer(amt)
    if amt is not None:
        print("Amount transffered. The balance is:", amt)
