a = 55555
b = 55566
c = 54454

with open("binaryfile","wb") as fh:
    fh.write(a.to_bytes(4,'big'))
    fh.write(b.to_bytes(4,'big'))
    fh.write(b.to_bytes(4,'big'))

with open("binaryfile","rb") as fh:
    print("a =",int.from_bytes(fh.read(4),"big"))
    print("b =",int.from_bytes(fh.read(4),"big"))
    print("c =",int.from_bytes(fh.read(4),"big"))
