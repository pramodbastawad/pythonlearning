# Python Learning
# Python Dictionary

def choose_option(ch):
    return {
        "Add":addition,
        "Substract":substract,
        "Multiply":multiply,
        "Division":division
        }[ch]

def addition():
    x, y = map(int, input("Enter the numbers to add:").strip().split())
    return x+y

def substract():
    x, y = map(int, input("Enter the numbers to add:").strip().split())
    return x-y

def multiply():
    x, y = map(int, input("Enter the numbers to add:").strip().split())
    return x*y

def division():
    x, y = map(int, input("Enter the numbers to add:").strip().split())
    return x/y

while True:
    switch_cases = {
        "0":"Quit",
        "1": "Add",
        "2": "Substract",
        "3": "Multiply",
        "4": "Division"
        }
    print("-"*40)
    for case in switch_cases:
        print("{c} : {v}".format(c=case, v=switch_cases[case]))

    print("-"*40)

    option = input("Please select your option:")
    if option == "0":
        break
    func = choose_option(switch_cases[option])
    print(func())
