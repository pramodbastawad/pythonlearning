# Python Learning
# Python Dictionary: Iterating python dictionaries

fruits={"apple":"An apple a day keeps doctor away",
        "Mango":"Mango is a seasonal fruit",
        "Orange":"A citrus fruit",
        "Grape":"Black and Green"        
        }

print(fruits)

for fruit in fruits:
    print(fruits[fruit])