#Find the neighbouring cells from a given position element

#Function to check the neighbouring elements
def CheckNeighbour(i, j, arr):
    narray = []
    row_limit = len(arr)
    if row_limit > 0:
        for r in range(max(0, i-1), min(i+2, row_limit)):
            col_limit = len(arr[r])
            for c in range(max(0, j-1), min(j+2, col_limit)):
                if i != r or j != c:
                    narray.append(arr[r][c])
    return narray

# Function to count the neighbours of the given element
def CountNeighbourOfElement(i, j, arr):
    count = 0
    row_limit = len(arr)
    if row_limit > 0:
        for r in range(max(0, i-1), min(i+2, row_limit)):
            col_limit = len(arr[r])
            for c in range(max(0, j-1), min(j+2, col_limit)):
                if i != r or j != c:
                    count += 1
    return count

# Function to count the neighbours of the given element
def CountNeighbourOfAll(arr):
    row_limit = len(arr)
    #countArray = [[0,] * len(arr[0])] * row_limit -->dont use this type of list creation.
    #https://stackoverflow.com/questions/2397141/how-to-initialize-a-two-dimensional-array-in-python
    # instead use the below one Eg. a = [[0 for x in range(columns)] for y in range(rows)]
    countArray = [[0 for c in range(len(arr[0]))] for r in range(row_limit)]
    for row in range(0, row_limit):
        col_limit = len(arr[row])
        for col in range(0, col_limit):
            countArray[row][col] = CountNeighbourOfElement(row, col, arr)
    return countArray

# Method to count neighbours
def compute_neigbours(Z):
    shape = len(Z), len(Z[0])
    #N = [[0,]*(shape[0])  for i in range(shape[1])]
    N = [[0 for j in range(shape[0])]  for i in range(shape[1])]
    for x in range(1, shape[0]-1):
        for y in range(1, shape[1]-1):
            N[x][y] = Z[x-1][y-1]+Z[x][y-1]+Z[x+1][y-1] \
                    + Z[x-1][y]            +Z[x+1][y]   \
                    + Z[x-1][y+1]+Z[x][y+1]+Z[x+1][y+1]
    return N

# Boiler plate code to call main method
if __name__ == '__main__':
    from time import process_time

    array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    y = list(map(int, input("Enter the position of the element:").strip().split()))

    if y[0] >= len(array) or y[1] >= len(array[0]):
        print("Enter valid range.")
        exit(0)
    
    posNbr = CheckNeighbour(y[0], y[1], array)
    print("Neighbours of ({0},{1}) are :{2}".format(y[0], y[1], posNbr))
    print("The total number of neighbours of element :{0} is:{1}"
          .format(array[y[0]][y[1]], CountNeighbourOfElement(y[0], y[1], array)))

    print("The count array matrix :")
    startTime = process_time()
    N = CountNeighbourOfAll(array)
    endTime = process_time() - startTime
    print("Time Consumed:",endTime)
    print(N)

    print("The count array matrix (different method):")
    startTime = process_time()
    N = compute_neigbours(array)
    endTime = process_time() - startTime
    print("Time Consumed:", endTime)
    print(N)
    # for r in range(0, len(N)):
    #     for c in range(0, len(N[r])):
    #         print(c, end=' ')
    #     print()
