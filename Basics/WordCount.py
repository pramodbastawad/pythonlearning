# Python Learning Lists,Dictionary

"""Wordcount exercise
1. For the --count flag, implement a print_words(filename) function that counts
how often each word appears in the text and prints:
word1 count1
word2 count2
...

Print the above list in order sorted by word (python will sort punctuation to
come before letters -- that's fine). Store all the words as lowercase,
so 'The' and 'the' count as the same word.

2. For the --topcount flag, implement a print_top(filename) which is similar
to print_words() but which prints just the top 20 most common words sorted
so the most common word is first, then the next most common, and so on.

"""

import sys

# Define print_words(filename) and print_top(filename) functions.
# You could write a helper utility function that reads a file
# and builds and returns a word/count dict for it.
# Then print_words() and print_top() can just call the utility function.

# Prints the words with count
def print_words(fileName):
    words = read_file(fileName)
    wordsDict = words_to_dict(words)
    print(wordsDict)

# Helper utility function that reads a file
def read_file(fileName):
    fileHandler = open(fileName, 'r')
    words = []
    for line in fileHandler:
        words += line.strip().split()
    return words

# Helper function to a word/count dict
def words_to_dict(words):
    wordsDict = {}
    for i in set(map(str.lower, words)):
        wordsDict[i] = list((map(str.lower, words))).count(i)
    return wordsDict

# Print the top word count
def print_top(filename):
    words = read_file(filename)
    wordsDict = words_to_dict(words)
    lists = sorted(wordsDict.items(), key=lambda i: i[1], reverse=True)
    print(lists)

# The main method to be called
def main():
    if len(sys.argv) != 3:
        print('usage: ./wordcount.py {--count | --topcount} file')
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print('unknown option: ' + option)
        sys.exit(1)

if __name__ == '__main__':
    main()
