# Python learning 
# Composition

class Point:
    def __init__(self,x ,y):
        self.x = x
        self.y = y

class Line:
    def __init__(self, x, y):
        self.pt1 = Point(x,y)
        self.pt2 = Point(y, x)
    def getLine(self):
        print("The coordinates of Line are:({0},{1}) and ({2},{3})".format(self.pt1.x,self.pt1.y, self.pt2.x,self.pt2.y) )
obj = Line(1,2)        
obj.getLine()



