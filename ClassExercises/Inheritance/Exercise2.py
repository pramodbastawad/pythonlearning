# Python Learning
# Inheritance Examples

class People:
    def __init__(self):
        self.name = ''
        self.addr = ''
        self.age = 0
        self.gender = ''
    

class Student(People):
    def __init__(self,n,ad,age,g):
        self.grades = 'A'
        self.fees = 500
        self.name = n
        self.addr = ad
        self.age = age
        self.gender =  g
    
    def getStudent(self):
        print("Name: {0} Address: {1}".format(self.name,self.addr))
        print("Fees: {0} Grades: {1}".format(self.fees,self.grades))

obj = Student('Stud1','Bangalore',20,'M')
obj.fees = '2000'
obj.grades = 'A+'

obj.getStudent()

