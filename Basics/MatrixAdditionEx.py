#Program to add two matrices

def addMatrix(a,b):
    c=[[0]*len(a[0]) for i in range(len(a))]
    for i in range(len(a)):
        for j in range(len(b)):
            c[i][j] = a[i][j] + b[i][j]  
    return c

if __name__ == '__main__':
    a =[[1,2],[4,5]]
    b =[[6,7],[8,9]]
    c=addMatrix(a,b)
    print("Addition of Matrices:")
    print("Matrix A=",a)
    print("Matrix B=",b)
    print("Matrix A+B=",c)
