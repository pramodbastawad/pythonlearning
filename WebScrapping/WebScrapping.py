import requests
from bs4 import BeautifulSoup

req = requests.get('http://pythonhow.com/example.html')
cont = req.content

print(cont)

soup = BeautifulSoup(cont,"html.parser")

print(soup.prettify())
