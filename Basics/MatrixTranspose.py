# Python Learning
# Program to transpose a matrix using nested loop

x = [[12, 7], [4, 5], [3, 8]]
y = [[0 for i in range(0, 3)] for j in range(0, 2)]

# for row in range(len(x)):
#     for col in range(len(x[0])):
#         y[col][row] = x[row][col]

y = [[x[col][row] for col in range(len(x))] for row in range(len(x[0]))]
print(y)
