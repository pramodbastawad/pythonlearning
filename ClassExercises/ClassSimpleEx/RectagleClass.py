# Python class and objects exercises
# A simple Rectangle class

# Class definition for Rectangle
class Rectangle:
    ''' A Simple rectangle class '''
    def __init__(self, length=1.0, width=1.0):
        self.length = length
        self.width = width

    def setLength(self, length):
        self.length = length

    def setWidth(self, width):
        self.width = width

    def getArea(self):
        return self.length * self.width

    def getPerimeter(self):
        return 2 * (self.length * self.width)

# Boile plate code to call main method
if __name__ == '__main__':
    obj = Rectangle()
    print("The area is: {0} and perimeter is: {1}".format(obj.getArea(), obj.getPerimeter()))

    obj.setLength(2)
    obj.setWidth(5)
    print("The area is: {0} and perimeter is: {1}".format(obj.getArea(), obj.getPerimeter()))

    obj = Rectangle(10, 20)
    print("The area is: {0} and perimeter is: {1}".format(obj.getArea(), obj.getPerimeter()))
