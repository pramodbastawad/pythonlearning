# Python Learning
# Multiple inhertance exercises 3

class A:
    def __init__(self):
        print("A.__init_()")

class B(A):
    def __init__(self):
        print("B.__init__()")
        super().__init__()

class C(A):
    def __init__(self):
        print("C.__init__")
        super().__init__()
class D(B,C):
    def __init__(self):
        self.i = 0
        print("d__init__")
        super().__init__()

    def fun(self):
        print("Hello")


# obj = D()
#obj = D()
#D.__delattr__(obj,"i")