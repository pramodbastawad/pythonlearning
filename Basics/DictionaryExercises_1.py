# Python learning: Dictionary

fruits={"apple":"An apple a day keeps doctor away",
        "Mango":"Mango is a seasonal fruit",
        "Orange":"A citrus fruit",
        "Grape":"Black and Green",
        "apple":"One more apple"
        }

print(fruits)

print("Delete the apple:")
del fruits["apple"]

print(fruits)

while True:
    dict_key = input("Enter a fruit:")
    if dict_key == 'quit':
        break
    # if dict_key in fruits:
    #     #discription = fruits[dict_key]
    #     discription = fruits.get(dict_key)
    #     print(discription)
    # else:
    #     print("We dont have a "+ dict_key)

    discription = fruits.get(dict_key,"We dont have a "+dict_key)
    print(discription)

