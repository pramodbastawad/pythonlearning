# The game of life by John Conway which is one of the earliest example of cellular automata
# Those cellular automaton can be conveniently considered as array of cells that are connected
# together through the notion of neighbours.
# This example is written in pythonic way
# The same problem is efficiently solved with numpy. I will be writting that in another python file.

# Method to count neighbours
def compute_neigbours(Z):
    shape = len(Z), len(Z[0])
    N  = [[0,]*(shape[0])  for i in range(shape[1])]
    for x in range(1,shape[0]-1):
        for y in range(1,shape[1]-1):
            N[x][y] = Z[x-1][y-1]+Z[x][y-1]+Z[x+1][y-1] \
                    + Z[x-1][y]            +Z[x+1][y]   \
                    + Z[x-1][y+1]+Z[x][y+1]+Z[x+1][y+1]
    return N

# Call main method
if __name__ == '__main__':
    Z = [[0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 0, 0],
         [0, 1, 0, 1, 0, 0],
         [0, 0, 1, 1, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0]]
    print(Z)
    X = compute_neigbours(Z)
    print(X)
    