# Python Classes and Objects
# OOP Composition excercise Still to work on this
import Author 

# Class definition for Book
class Book:
    def __init__(self):
        self.name = ''
        self.author = Author.Author()
        self.price = 0.0
        self.quantity = 0

    def getName(self):
        return self.name

    def getAuthor(self):
        return self.author.name

if __name__ == '__main__':
    obj = Book()    
    obj.name = input('Enter name of the book:')
    print("Enter author details:")
    obj.author.name = input('Author Name:')
    obj.author.email = input('Author Email:')
    obj.author.gender = input('Author Gender:')
    obj.price = input('Enter name of the price:')
    obj.quantity = input('Enter name of the quantity:')
    print("Author:", obj.getAuthor())
