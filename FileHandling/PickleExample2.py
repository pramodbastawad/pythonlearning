import pickle

books = (
    'python',
    [
        ('author','xyz'),
        ('Publication','yyyy')
    ]
)

print(books)

with open("pickleex.txt","wb") as fh:
    pickle.dump(books,fh,protocol=0)

with open("pickleex.txt","rb") as fh:
    pickle.load(fh)