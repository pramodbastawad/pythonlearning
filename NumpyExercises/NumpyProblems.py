# Python learning Numpy Exercises 
import numpy as np

def sumofproducts(x, y):
    """ Return the sum of x[i] * y[i] for all pairs of indicees i, j """
    result = 0
    result = np.sum(x) * np.sum(y)
    # for i in range(len(x)):
    #     for j in range(len(y)):
    #         result += x[i] * y[j]
    return result

def countLower(x, y):
    """ Return the number of pairs i, j such that x[i] < y[j] """
    result = 0
    # for i in range(len(x)):
    #     for j in range(len(y)):
    #         if x[i] < y[j]:
    #             result += 1
    result = np.sum(np.less(x,y))
    # result = np.sum(np.searchsorted(np.sort(x),y))
    
    return result

def cleanup(x, missing=-1, value=0):
    """Return an array that's the same as x, except that where x ==
    missing, it has value instead.

    >>> cleanup(np.arange(-3, 3), value=10)
    ... # doctest: +NORMALIZE_WHITESPACE
    array([-3, -2, 10, 0, 1, 2])

    """
    result = []
    # for i in range(len(x)):
    #     if x[i] == missing:
    #         result.append(value)
    #     else:
    #         result.append(x[i])
    # return np.array(result)
    result = np.where(x == missing, value, x)
    return result

if __name__ == '__main__':
    a = np.array(np.arange(3000), dtype=np.int64)
    b = np.array(np.arange(3000), dtype=np.int64)
    #print(sumofproducts(np.arange(3000,dtype=int), np.arange(3000,dtype=int64)))
    print("Sum of ProductsCount:", sumofproducts(a, b))


    a = np.arange(0, 200, 2)
    b = np.arange(40, 140)
    print("Count lower:", countLower(a, b))

    print("CleanUp :", cleanup(np.arange(-3, 3), value=10))
    