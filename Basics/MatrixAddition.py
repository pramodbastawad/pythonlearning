# Python learning Lists, Loops
#Program to add two matrices

x = [[12,7,3],[4,5,6],[7,8,9]]
y = [[5,8,1],[6,7,3],[4,5,9]]
#z = [[0]*3]*3
z = [[0 for a in range(len(x[0]))] for b in range(len(x))]
print(z)
#using normal for loop
# for i in range(len(x)):
#     for j in range(len(x[0])):
#         z[i][j] = x[i][j] + y[i][j]

#using list comprehension
z = [[x[row][col]+y[row][col] for col in range(len(x[0]))] for row in range(len(x))]

print(z)
