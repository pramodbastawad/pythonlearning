#Program to work on file handling
from sys import argv

# Write input string to the file
def writeToFile(filename, inputstr):
    '''Function to write to the given file'''
    with open(filename, 'w') as fh:
        fh.write(inputstr)

# Read content from the file
def readFromFile(filename):
    ''' Function to read from a given file'''
    with open(filename, 'r') as fh:
        print(fh.read())

# The main method
def main():
    if len(argv) < 2:
        print("Enter valid input parameters:\n Syntax: FileExercise <file_name>")
        exit(0)
    fileName = argv[1]
    inputString = input("Enter your details:")
    writeToFile(fileName, inputString)
    readFromFile(fileName)

# The boiler plate code to begin the execution
if __name__ == "__main__":
    main()
