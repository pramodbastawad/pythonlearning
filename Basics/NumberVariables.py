#!/bin/python3

# Given five positive integers, find the minimum and maximum values that can be
# calculated by summing exactly four of the five integers.
# Then print the respective minimum and maximum values as a single line of two
# space-separated long integers.
# Input Format
##A single line of five space-separated integers.
# Constraints
# #Each integer is in the inclusive range .
# Output Format
## Print two space-separated long integers denoting the respective minimum and
## maximum values that can be calculated by summing exactly four of the five integers.
## (The output can be greater than 32 bit integer.)

# Read input from console
arr = list(map(int, input().strip().split(' ')))
arrLen = len(arr)
i = 0
result = []

# Loop through the array to get the sum
while i < arrLen:
    sumvalue = 0
    for item in arr:
        if item != arr[i]:
            sumvalue += item
    result.append(sumvalue)
    i += 1

# Sort the list
result.sort()

# Print the result
print(result[0], result[-1])
