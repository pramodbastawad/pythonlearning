#Python learning 
#File hanlding: Binary file read write

# reading and writing with bytes
with open("binary",'bw') as binFile:
    binFile.write(bytes(range(10)))
with open("binary","rb") as binFile:
    for i in binFile:
        print(i)

# reading writing with to_bytes and from_bytes
a = 65534
b = 65535
c = 65536
d = 2998302

with open('binary2','wb') as binFiles:
    binFiles.write(a.to_bytes(4,"big"))
    binFiles.write(b.to_bytes(4,"big"))
    binFiles.write(c.to_bytes(4,"big"))
    binFiles.write(d.to_bytes(8,"big"))

with open('binary2','rb') as binFil:
    print("a = ",int.from_bytes(binFil.read(4),'big'))
    print("b = ",int.from_bytes(binFil.read(4),'big'))
    print("c = ",int.from_bytes(binFil.read(4),'big'))
    print("d = ",int.from_bytes(binFil.read(8),'big'))
