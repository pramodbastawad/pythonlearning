# "There should be one-- and preferably only one --obvious way to do it."
class Prop:
    def __init__(self, x):
        self.__x = x

    def __get_x(self):
        return self.__x

    def __set_x(self, x):
        self.__x = x
    
    x = property(__get_x, __set_x)
    # @property
    # def x(self):
    #     return self.__x

    # @x.setter
    # def x(self,x):
    #     self.__x = x