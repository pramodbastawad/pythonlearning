# Python Classes and Objects
# OOP Composition excercise

class Author:
    def __init__(self):
        self.name = ''
        self.email = ''
        self.gender = 'm'

    def setName(self, name):
        self.name = name

    def setEmail(self, email):
        self.email = email

    def setGender(self, gender):
        self.gender = gender

    def getAuthor(self):
        print("Name: ", self.name)
        print("Email: ", self.email)
        print("Name: ", self.gender)
