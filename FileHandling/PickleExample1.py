import pickle

# Python learning
# File IO: pickle Object serialization

mobiles = ('Micromax',
           'Canvas',
           6999,
           (('32GB', '1.4 GHz'),
           ('16 MP','13MP') ,
           ('Android', 'Dual')))

# with open('mobiles','wb') as pickleFile:
#     pickle.dump(mobiles,pickleFile)

# with open('mobiles','rb') as pickleRead:
#     mobiles1 = pickle.load(pickleRead)
even = list(range(0,10,2))
odd = list(range(1,10,2))

with open('mobiles','wb') as pickleFile:
    pickle.dump(mobiles,pickleFile)
    pickle.dump(even,pickleFile)
    pickle.dump(odd,pickleFile)

with open('mobiles','rb') as pickleRead:
    mobiles1 = pickle.load(pickleRead)
    evens = pickle.load(pickleRead)
    odds = pickle.load(pickleRead)

brand, model, price, features = mobiles1

print("Brand:",brand)
print("Model:",model)
print("Price:",price)
print("Features:",features)

print("-"*40)
for i in evens:
    print(i,end=' ')

print()
print("-"*40)
for i in odds:
    print(i,end=' ')

# class test:
#     attr = 'Test pickle object'

# testObj = pickle.dumps(test)
# print(testObj)
# ptestObj = pickle.loads(testObj)
# print(ptestObj.attr)
