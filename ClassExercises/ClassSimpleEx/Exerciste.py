#Python Learning

# Basics of Class

class Mobile:
    def __init__(self, m = None, p= None):
        if m is None and p is None:
            self.model = 'DefModel'
            self.price = '1000'
        else:
            self.model = m
            self.price = p
    
    def getMobile(self):
        print("Model:",self.model)
        print("Price:",self.price)
    
    def setMobile(self, m, p):
        self.model = m
        self.price = p

obj = Mobile()

obj.getMobile()
obj.setMobile('samsung',2000)
obj.getMobile()
