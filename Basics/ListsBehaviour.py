#Python append() vs. + operator on lists, why do these give different results?
def proc1(p):
    p.append(1)
x = [2, 5]
proc1(x)
print(x)


def proc2(p):
    p = p+[1]
    print("In proc2 ", p)
y = [2, 5]
proc2(y)
print(y)


z = [2, 5]
z = z + [1]
print(z)
