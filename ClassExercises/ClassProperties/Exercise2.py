# Python Learning
# properties
# Two things are noteworthy: 
# We just put the code line "self.x = x" in the __init__ method 
# and the property method x is used to check the limits of the values. 
# The second interesting thing is that we wrote "two" methods with 
# the same name and a different number of parameters 
# "def x(self)" and "def x(self,x)". 
# There is still another problem in the most recent version. We have now two ways to access or change the value of x: Either by using "p1.x = 42" or by "p1.set_x(42)". This way we are violating one of the fundamentals of Python: "There should be one-- and preferably only one --obvious way to do it." (see Zen of Python) 



# class P:
#     def __init__(self,x):
#         self.x = x
    
#     @property
#     def x(self):
#         return self.__x

#     @x.setter
#     def x(self, x):
#         if x < 0:
#             self.__x = 0
#         elif x > 1000:
#             self.__x = 1000
#         else:
#             self.__x = x

# class P:
#     def __init__(self,x):
#         self.set_x(x)

#     def get_x(self):
#         return self.__x

#     def set_x(self, x):
#         if x < 0:
#             self.__x = 0
#         elif x > 1000:
#             self.__x = 1000
#         else:
#             self.__x = x

#     x = property(get_x, set_x)       

class P:

    def __init__(self,x):
        self.__set_x(x)

    def __get_x(self):
        return self.__x

    def __set_x(self, x):
        if x < 0:
            self.__x = 0
        elif x > 1000:
            self.__x = 1000
        else:
            self.__x = x

    x = property(__get_x, __set_x)

