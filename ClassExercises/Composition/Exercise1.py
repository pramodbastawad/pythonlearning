# Python learning 
# Classes : Composistion 

class Salary:
    def __init__(self,pay):
        self.pay = pay

    def get_total(self):
        return self.pay * 12

class Employee:
    def __init__(self, pay, bonus):
        self.sal = Salary(pay)
        self.bonus = bonus
    
    def annual_salary(self):
        return "Total: " + str(self.sal.get_total() + self.bonus)

if __name__  == '__main__':
    obj  = Employee(200,100)
    print(obj.annual_salary())
