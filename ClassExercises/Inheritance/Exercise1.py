# Python Learning
# Class inheritance

class Circle:
    def __init__(self, r = None, c = None):
        if r is None and c is None:
            self.radius = 1.0
            self.color = 'red'            
        else:
            self.radius = r
            self.color = c

    def getArea(self):
        return 3.142 * self.radius * self.radius

class Cylinder(Circle):
    def __init__(self, h, r, c):
        self.height = h
        self.radius = r
        self.color = c
    
    def getVolume(self):
        return self.getArea() * self.height

obj = Cylinder(2, 2, 'blue')
print(obj.getVolume())
