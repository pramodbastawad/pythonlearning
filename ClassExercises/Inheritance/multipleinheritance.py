# Python Learning
# Multiple inhertance

class A:
    def func(self):
        print("I am Class A")

class B:
    def func(self):
        print("I am Class B")

class C(B,A):
    def func(self):  
        A.func(self)      
        print("I am class C")

obj = C()
obj.func()
if issubclass(C,B):
    print("B is super class of C")
else:
    print("Wrong")