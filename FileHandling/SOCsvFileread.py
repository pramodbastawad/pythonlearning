import csv
import requests

CSV_URL = 'https://query1.finance.yahoo.com/v7/finance/download/BATS.L?period1=789091200&period2=1512950400&interval=1d&events=history&crumb=OaD0j8/Z7YO'


with requests.Session() as s:
    download = s.get(CSV_URL)

    decoded_content = download.content.decode('utf-8')

    cr = csv.reader(decoded_content.splitlines(), delimiter=',')
    my_list = list(cr)
    print("printing...")
    for row in my_list:
        print(row)