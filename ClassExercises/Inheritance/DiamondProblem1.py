class A:
    def fun(self):
        print("I am Class A method")

class B(A):
    def fun(self):
        print("I am Class B method")
        A.fun(self)
    

class C(A):
    def fun(self):
        print("I am Class C method")
        A.fun(self)

class D(B,C):
    def fun(self):
        print("I am Class D method")
        B.fun(self)
        C.fun(self)
        # A.fun(self)