# Python Learning
# File operations

# Program to append following
# 2 times of 1 is 2
# 2 times of 2 is 4
# 2 times of 3 is 6
# 2 times of 4 is 8 etc

with open("sample1.txt",'a') as table:
    for i in range(10):
        print("{1} times of {0} is {2}".format(2, i, i*2),file=table)
