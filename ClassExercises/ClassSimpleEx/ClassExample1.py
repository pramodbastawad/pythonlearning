#Classes
class MyClass:
    """ A simple example class"""
    i = 1234

    def fun(self):
        return "Hello world!"

if __name__ == '__main__':
    x = MyClass()
    print(x.fun())
    x.i = "5678"
    print(x.i)
    print(x.__doc__)
