# Basic string exercises
# 1. Palindrome String Check

# Method to check for palindrome
def checkforpalindrome(pstr):
    ''' Function to check whether the given string is palindrom or not'''
    start = 0
    end = len(pstr) - 1
    flag = True
    while start != end:
        if pstr[start] == pstr[end]:
            start += 1
            end -= 1
        else:
            flag = False
            break
    return flag

#Boiler plate code to call main method
if __name__ == "__main__":
    data = input("Enter the string:")
    if checkforpalindrome(data) is True:
        print("Strings are pallindrome")
    else:
        print("Strings are not pallindrome")
