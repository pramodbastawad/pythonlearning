# Python classes and objects exercises
# Simple Circle class

class Circle:
    # class variable shared by all instances
    i = 456
    def __init__(self, r=1.0):
        #self.radius: instance variable unique to each instance
        self.radius = r
        self.color = 'Red'

    def getRadius(self):
        return self.radius

    def getArea(self):
        import math
        return self.radius ** 2 * math.pi

    def getCircumference(self):
        '''C=2πr'''
        import math
        return 2 * math.pi * self.radius

    def setRedius(self, rad):
        self.radius = rad

    def setColor(self, col):
        self.color = col

if __name__ == '__main__':
    obj = Circle()
    print("The Circle has radius of:{0} and area of: {1}".format(obj.getRadius(), obj.getArea()))

    obj = Circle(5.0)
    print("The Circle has radius of:{0} and area of: {1}".format(obj.getRadius(), obj.getArea()))

    obj = Circle(6.0)
    print("The Circle has radius of:{0}, area of: {1} and circumference of: {2}".format
          (obj.getRadius(), obj.getArea(), obj.getCircumference()))

    # #Instance object : data attributes
    # obj.x = "this is simple test"
    # print(obj.x)

    # # The other kind of instance attribute reference is a method.
    # # A method is a function that “belongs to” an object.
    # print(obj.getArea())

    # #Method object
    # print(Circle.getArea(obj))

    r = input("Set the radius:")
    obj.setRedius(r)
    print("The radius is set to :{0}".format(obj.getRadius()))

    c = input("Set the Color:")
    obj.setColor(c)
    print("The color is set to :{0}".format(obj.color))
